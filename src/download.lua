
local GIT_API_URL    = "https://codeberg.org/api/v1/repos/";
local GIT_AUTHOR     = "sovnya";
local GIT_REPO       = "storage-cell-cc";
local GIT_FULL_URL   = GIT_API_URL .. GIT_AUTHOR .. "/" .. GIT_REPO .. "/contents/";
local GIT_BRANCH     = "main"

local CLIENT_URL     = GIT_FULL_URL .. "src/client" .. "?ref=" .. GIT_BRANCH;
local SERVER_URL     = GIT_FULL_URL .. "src/server" .. "?ref=" .. GIT_BRANCH;
local API_URL        = GIT_FULL_URL .. "src/api" .. "?ref=" .. GIT_BRANCH;

function main()
    local to_download = arg[1];

    if to_download == "client" then 
        local client_ok, err = http.checkURL(CLIENT_URL);
        local api_ok, err = http.checkURL(API_URL);
        if not client_ok or not api_ok then
            print("Error: " .. err)
            return
        end

        local client_request = http.get(CLIENT_URL);
        local api_request = http.get(API_URL);

        local client_content_json = textutils.unserialiseJSON(client_request.readAll());
        local api_content_json = textutils.unserialiseJSON(api_request.readAll());

        -- download client files
        for i = 1, #client_content_json do
            local file_name = client_content_json[i].name;
            local file_url = client_content_json[i].download_url;
            local file_content = http.get(file_url);
            local file_content_str = file_content.readAll();
            local file_path = "client/" .. file_name;
            local file_handle = fs.open(file_path, "w")
            file_handle.write(file_content_str);
            file_handle.close();
        end
        print("Downloaded client.");

        -- download api files
        for i = 1, #api_content_json do
            local file_name = api_content_json[i].name;
            local file_url = api_content_json[i].download_url;
            local file_content = http.get(file_url);
            local file_content_str = file_content.readAll();
            local file_path = "client/api/" .. file_name;
            local file_handle = fs.open(file_path, "w");
            file_handle.write(file_content_str);
            file_handle.close()
        end
        print("Downloaded API.");
    end

    if to_download == "server" then
        local server_ok, err = http.checkURL(SERVER_URL);
        local api_ok, err = http.checkURL(API_URL);
        if not server_ok or not api_ok then
            print("Error: " .. err)
            return
        end

        local server_request = http.get(SERVER_URL);
        local api_request = http.get(API_URL);

        local server_content_json = textutils.unserialiseJSON(server_request.readAll());
        local api_content_json = textutils.unserialiseJSON(api_request.readAll());

        -- download server files
        for i = 1, #server_content_json do
            local file_name = server_content_json[i].name;
            local file_url = server_content_json[i].download_url;
            local file_content = http.get(file_url);
            local file_content_str = file_content.readAll();
            local file_path = "server/" .. file_name;
            local file_handle = fs.open(file_path, "w");
            file_handle.write(file_content_str);
            file_handle.close();
        end
        print("Downloaded server.");

        -- download api files
        for i = 1, #api_content_json do
            local file_name = api_content_json[i].name;
            local file_url = api_content_json[i].download_url;
            local file_content = http.get(file_url);
            local file_content_str = file_content.readAll();
            local file_path = "server/api/" .. file_name;
            local file_handle = fs.open(file_path, "w");
            file_handle.write(file_content_str);
            file_handle.close();
        end
        print("Downloaded API.");
    end
end

main();