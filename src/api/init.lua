-- API
local client = require("api.client")
local server = require("api.server")

return {
    client = client,
    server = server
}