local chests_api = {}
local monitor_api = {}

--------------------------------------------------------------------------------------------

function chests_api.getChests()
    local chests = peripheral.getNames()
    local actual_chests = {}

    for i, chest in ipairs(chests) do
        if chest:find("chest") or chest:find("barrel") then
            actual_chests[i] = chest
        end
    end

    return actual_chests
end

function chests_api.getItemList(chest)
    return peripheral.call(chest, "list")
end

function chests_api.getItemDetail(chest, slot) 
    return peripheral.call(chest, "getItemDetail", slot)
end

--------------------------------------------------------------------------------------------

function monitor_api.init() 
    local monitor_inner = {}
    local top_monitor = peripheral.wrap("top")
    local main_monitor = nil

    -- get connected monitor
    local peripherals = peripheral.getNames()
    for i, v in pairs(peripherals) do
        local name = peripheral.getName(peripheral.wrap(v))
        if name:find("monitor_") then
            main_monitor = peripheral.wrap(v)
        end
    end

    function monitor_inner:about() top_monitor.write("Running " .. os.version() .. " on " .. _HOST) end

    function monitor_inner:write(text, x, y)
        main_monitor.setCursorPos(x, y) 
        main_monitor.write(text) 
    end

    function monitor_inner:write_items() 
        --main_monitor.clear()
        main_monitor.setCursorPos(1, 1)
        local chests = chests_api.getChests()

        for i, chest in ipairs(chests) do
            local list = chests_api.getItemList(chest)
            for i, slot in ipairs(list) do
                local details = chests_api.getItemDetail(chest, i)
                self:write(("- %s: %d"):format(details.displayName, details.count), 1, i)
            end
        end
    end

    return monitor_inner
end

--------------------------------------------------------------------------------------------

return {
    chests = chests_api,
    monitor = monitor_api
}