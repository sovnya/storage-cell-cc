-- client.lua
local api = require("api").client

local monitor = api.monitor.init()
monitor:about()

while true do
    monitor:write_items()
    os.sleep(1)
end