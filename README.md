# storage-cell-cc
TBD

# Installation
Just run this command in the computer
```
wget https://codeberg.org/sovnya/storage-cell-cc/raw/branch/main/src/download.lua
```

## Downloading the client
Just run the following command in the computer
```
download client
```
And then you can run the client by typing `client/client.lua` in the computer

## Downloading the server
Just run the following command in the computer
```
download server
```
And then you can run the server by typing `server/server.lua` in the computer